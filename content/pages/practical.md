---
title: Practical info
slug: practical
default_state: 'off'
column: 3
order: 7
overflow: scroll
---

## Locations

**Q-O2** Quai des Charbonnages/Koolmijnenkaai 30-34, 1080 Sint-Jans-Molenbeek

**Decoratelier** Rue de Manchester/Manchesterstraat 17, 1080 Sint-Jans-Molenbeek

**Atoma** Rue Pierre Decoster/Pierre Decosterstraat 110, 1190 Vorst

**Zonneklopper** Avenue de la Verrerie/ Glasblazerijlaan 23A, 1190 Vorst

**Abbaye de Forest** Place Saint-Denis/Sint-Denijs Plein 9, 1190 Forest/Vorst

**Q-O2** is an arts laboratory for experimental music and sound art. Since 2006, the organisation has its own space in the Brussels canal zone. Here, we organise residencies for artists, focussing on artistic research and reflection over the production of finished works. The public output of this research takes a variety of forms including showings, concerts, thematic projects, symposia, publications, workshops, and the annual festival Oscillation. Q-O2 practices collaborative and transversal methods, and we welcome cross-overs into other disciplines, and various urban and social contexts, as well as collaborations with both local and international partners.

[q-o2.be](http://www.q-o2.be/nl/)

**Decoratelier** was founded in 2016, by Jozef Wouters and Menno Vandevelde. Located in an old factory building in Brussels, it is the base for various projects and artistic collaborations and an accessible workplace for artists from various disciplines. Space is tested, conversations take place in wood, iron and cardboard. It is a permanent place within the constructional quest for constantly changing spaces in which art, thought and artisans meet.

[somethingatdecoratelier.brussels](http://somethingatdecoratelier.brussels)

**ATOMA** is a collectively-run space with workshops (wood, metal, music, sound, visual, photography, screen printing, brewery, bakery, chocolate...) and a living space. Like any collectively-run space, ATOMA looks for methods for collaborative organisation, fluid and sincere, on how to be together, how to talk to each other etc. Atoma is also a public space. During its events—which range from concerts to screenings to events linked to the workshops of the space—the fragile notion of experience and experiment with others emerges.

**Zonneklopper** is a collective project formed around a temporary occupation in Forest. Here, they experiment and shape new forms of self-organisation for activism, art, culture, and society. A varied and diverse village where public events such as concerts, performances and exhibitions are regularly hosted, artist’s ateliers are provided, and working spaces are shared, together with housing and accommodation for migrants in transit. Zonneklopper is self-managed and all the resources are shared and mutualised in a collective perspective.

[zonneklopper.net](http://www.zonneklopper.net)


## Credits

Oscillation is a festival by [Q-O2.be](https://Q-O2.be).

Q-O2 is supported by the Flemish Government  and the Vlaamse Gemeenschapscommissie.

Festival web development & design by Open Source Publishing [OSP](http://osp.kitchen).
Fonts used are MetaAccanthis by Amélie Dumont, and Karrik by Jean-Baptiste Morizot & Lucas Le Bihan, both under SIL Open Font License.

Radio partners: Shirley & Spinoza, The Listening Arts Channel, Cashmere Radio, Kiosk Radio, Radio LYL, Radio Helsinki, Radio Panik, Radio Campus Bruxelles

Festival Team: Julia Eckhardt, Caroline Profanter, Henry Andersen, Ludo Engels, Dries Robbe, Margherita Brillada, Amar Ruiz, and Christel Simons.

Contact: [info@q-o2.be](mailto:info@q-o2.be)

[Previous editions](http://www.oscillation-festival.be/start.html)
