---
title: Infos pratiques
slug: practical
lang: fr
---

## Sites

**Q-O2** Quai des Charbonnages 30-34, 1080 Molenbeek-Saint-Jean

**Decoratelier** Rue de Manchester 17, 1080 Molenbeek-Saint-Jean

**Atoma** Rue Pierre Decoster 110, 1190 Forest

**Abbaye de Forest** Place Saint-Denis 9, 1190 Forest

**Zonneklopper** Avenue de la Verrerie 23A, 1190 Forest

**Q-O2** est un laboratoire artistique pour musique expérimentale et art sonore. Depuis 2006, l'organisation dispose de son propre espace aux alentours du canal de Bruxelles. Ici, sont organisées des résidences d'artistes, axées sur la recherche artistique et la réflexion sur la production d'œuvres finies. La présentation publique de cette recherche prend diverses formes, notamment expositions, concerts, projets thématiques, symposiums, publications, workshops et le festival annuel Oscillation. Q-O2 pratique des méthodes collaboratives et transversales, et accueille des croisements avec d'autres disciplines, et divers contextes urbains et sociaux, ainsi que des collaborations avec des partenaires locaux et internationaux.

[q-o2.be](http://www.q-o2.be/)

**Decoratelier** a été fondé en 2016 par Jozef Wouters et Menno Vandevelde. Situé dans une ancienne usine à Bruxelles, il est la base de divers projets et collaborations artistiques et un lieu de travail accessible à des artistes de diverses disciplines. L'espace est remis en question, les conversations se déroulent dans le bois, le fer et le carton. C'est un lieu permanent dans la quête constructive d'espaces en constante évolution où l'art, la pensée et l'artisanat se rencontrent.

[somethingatdecoratelier.brussels](http://somethingatdecoratelier.brussels)

**ATOMA** est un espace géré collectivement avec des ateliers (bois, métal, musique, son, visuel, photographie, sérigraphie, brasserie, boulangerie, chocolaterie...) et un espace de vie. Comme tout espace géré collectivement, ATOMA recherche des modes d'organisation collaboratif, fluides et sincères, sur comment être ensemble, comment se parler etc. Atoma est aussi un espace public. Au cours de ses événements – qui vont des concerts aux projections en passant par des événements liés aux ateliers du lieu – émerge la fragile notion d'expérience et d'expérimentation avec les autres.

**Zonneklopper** est un projet collectif formé autour d'une occupation temporaire à Forest. Ici, ils expérimentent et façonnent de nouvelles formes d'auto-organisation pour l'activisme, l'art, la culture et la société. Un village varié et diversifié où des événements publics tels que des concerts, des performances et des expositions sont régulièrement organisés, des ateliers d'artistes sont fournis et des espaces de travail sont partagés, ainsi que des logements et des hébergements pour des migrants en transit. Zonneklopper est autogéré et toutes les ressources sont partagées et mutualisées dans une perspective collective.

[zonneklopper.net](http://www.zonneklopper.net)

## Crédits

Oscillation est une festival de [Q-O2.be](https://Q-O2.be).

Q-O2 est soutenu par le Gouvernement flamand et la Vlaamse Gemeenschapscommissie.

Web development & design par [OSP](http://osp.kitchen).
Ce site utilise les fonts MetaAccanthis par Amélie Dumont, et Karrik par Jean-Baptiste Morizot & Lucas Le Bihan, toutes les deux sous SIL Open Font License.

Radio partners: Shirley & Spinoza, The Listening Arts Channel, Cashmere Radio, Kiosk Radio, Radio LYL, Radio Helsinki, Radio Panik, Radio Campus Bruxelles

Festival Team: Julia Eckhardt, Caroline Profanter, Henry Andersen, Ludo Engels, Dries Robbe, Margherita Brillada, Amar Ruiz, et Christel Simons.

Contact: [info@q-o2.be](mailto:info@q-o2.be)

[Éditions précédentes](http://www.oscillation-festival.be/start.html)
