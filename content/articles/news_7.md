---
title: The Oscillation Bulletin Episode 7
date: 04-30
slug: news-7
image: './images/OSCILLATION_29.04_CP15.jpg'
image_credits: 'Ryoko Akama & Anne-F Jacques last night at Atoma. © Camille Poitevin'
section: blog
---



Dear addressed public,

We are still searching for our sense of time and space after last night's tour de force at Atoma ::: please, don't ever take us off this heavenly cloud. Today we are down to two new venues: the Abbaye de Forest and Zonneklopper in Forest. We provided an—open air—afternoon program, so don't hesitate to arrive early.

At 14:00, David Helbich's Figures of Walking installation opens at the Abbaye De Forest. His score of chalk on grass in up to three tracks for any number of people is a social choreography, with figures and patterns inspired by concepts of institutionally and intuitively organized walking in groups, such as in dance or military drill. The individual and collective behavior of the self-performing participants sheds light on empowerment within such given structures.

Alisa Oleva is hosting the last of her amazing walks today, starting at Zonneklopper at 15:00. If no one has told you about it until now: we guarantee you, this is an experience that will stay with you for a long time.

Amber Meulenijzer and Pak Yan Lau will collaborate to create a composition and live performance tailored to the grandiose SAAB SCULPTURES soundsystem, carried by a Saab 900 car that is placed in different contexts—questioning, transforming and pimping public/private space. Their performance starts at 16:00, also at the Abbaye De Forest.

They are followed by De zwarte zusters, who will make a —presumably— wild improvisation on sound, image and the narrative of De zwarte zusters as a community. During their performance, they explore in  the tension between the musical and the visual performative aspect, in which the search for collective values, friendship and trust are central. Playing football, running in circles around the audience, ... anything can really happen.

At 17:30, we're opening the doors of Zonneklopper, where food will also be available. Half an our later we're hosting a round table with  Alisa Oleva, Bill Dietz, RYBN and David Helbich, moderated by Elena Biserna. The round table is an occasion to discuss together about formats and apparatuses that destabilize traditional relationships between artist and audience through movement, walks, itineraries, and collective or private spatial explorations.

After a short break, Céline Gillain will talk about how listening to music is conditioned by context, based on her own experience as a performer. Performing in a music festival, an art institution, a concert hall or a club are very different experiences, with different conditions, sets of rules and expectations—she's interested in questioning the cleavage this creates between audiences and how it is rooted in issues related to class, sex and race.

The first musical performance of the night is an improvised concert by Peter Kutin and accordeonist Stan Maris at 20:30. The sound of the latter will be implemented and counterbalanced by Kutin’s Light-to-Noise Feedback Systems—sound and light will cast shadows.

Next up is Mariam Rezaei. Using two turntables, she improvises with vocal and instrumental samples in an improvisation bridging her Iranian and English heritage. Timbres moves between reductionism, noise, free jazz and absurdism with hints of opera and hiphop.

At 22:15 Thomas Ankersmit takes the stage. He uses PA systems and his Serge Modular analog synthesizer to activate real, physical spaces and bodies with sound, or rather to suggest imaginary spaces, often in contrast to each other. Inspired by the work of e.g. Maryanne Amacher and Dick Raaijmakers, he explores the potential of resonant frequencies and oto-acoustic emissions to 'hack' simple stereo PA systems for a more physical, three-dimensional way of listening. 

The icing on the cake for this night is the infamous DJ Marcelle. 'Well known for her three-turntable setup, DJ Marcelle makes compositions out of songs and symphonies out of mixes — colliding disparate genres, appropriated vocal snippets and warped soundscapes into a giant Frankenstein-like melting pot.' - RA. We don't have anything to add to that.
