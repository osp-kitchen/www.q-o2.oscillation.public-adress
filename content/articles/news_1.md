---
title: The Oscillation Bulletin Episode 1
date: 04-8
slug: news-1
image: 
image_credits:
section: blog
---

Dear all,

Right now, you are reading the very first sentence of this year's series of Oscillation Bulletins. The next three weeks, many more will follow, and will keep you regularly updated about our Oscillation ::: Public Address Festival.

The website and full festival program are online now, available in English, Nederlands and français. The posting of this bulletin also marks the official start of the ticket sales, both for the workshops as for the festival. Be sure to take your time to discover our new website, the designers and web developers at OSP made us feel rather proud about it.

After two years of remote festival versions, it's about time to meet each other again; the concerts, sound walks, talks and workshops will all be spread throughout Brussels. Our hosts for this year are the ever nice Decoratelier near our base in Sint-Jans-Molenbeek, and Atoma and Zonneklopper in Vorst/Forest. Fully in line with the focus of this edition, the surrounding public spaces of these locations will also be used.

So, time to dig deeply in our program and discover what artists like RYBN, Enrico Malatesta & Attila Faravelli, David Helbich, Jasmine Guffond, Kate Carr, Mariam Rezaei, Marta De Pascalis, Matana Roberts, DJ Marcelle, Thomas Ankersmit, Collective Actions Group, Pak Yan Lau & Amber Meulenijzer and many more have to offer for you. This year's workshops are held by Margherita Brillada (Radiophonix), Lia Mazzari (Whip Cracking), Elena Biserna (Feminist Steps), Alisa Oleva (A Listening Walkshop), Attila Faravelli  (Aural Tools), and walks by RYBN, Alisa Oleva and Jérôme Giller.

**To contribute a performance to the The Open Mic Night on Friday 29/4**, please send a 1 paragraph proposal, as well as technical requirements to margherita@q-o2.be before April 22. Sets are limited to 5 minutes and should be acoustic, limited to a single microphone or able to be installed direct-to-mixer without sound check ('plug and play'). Performers will receive a ticket to the festival for Friday 29/4 and drink vouchers.

Kind regards,

The Oscillation Crew
