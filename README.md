# Q-O2 Oscillation Public Address - Website

Website for the fourth edition of the Oscillation festival by Q-02, on the theme of Public Adress.
This is a static file website generated with Pelican.

## Dependencies (for dev)

pelican + markdown + yaml

    pip install "pelican[markdown]" yaml2

markdown extension: [markdown-newtab](https://github.com/pehala/markdown-newtab)

    pip install markdown3-newtab

pelican plugin: [pelican-image-process](https://github.com/pelican-plugins/image-process).
it's in the `plugins` folder, but has dependencies to install:

    pip install Pillow bs4

yaml pelican plugin: [pelican-yaml-metadata](https://github.com/pR0Ps/pelican-yaml-metadata). it's in the `plugins` folder nothing to do!


## How to edit the website content

### commit changes and website generation

The website content is located in the `content/` folder.

All the sections (like _about_, _line-up_, etc) are in a separate file in the `pages/` subfolder.
Every blog posts and specials are a separate file in the `articles/` subfolder.
Images must be put in the `images/` folder.

To edit a section:
1. click on the file name to open it
2. click on the down arrow in the blue button on the top right and select **Edit**
3. click on **Edit**
4. edit the content
5. scroll down and click **Commit changes**, note that you can name your commit according to what kind of changes you made.

The website will automatically update but it can take 1 or 2 minutes.
If you want to inspect the process you can go in **CI/CD** on the left menu, then open the **jobs** tab.
It should list all the previous changes, with either _runing_ or _done_.

The website address is [osp-kitchen.gitlab.io/www.q-o2.oscillation.public-adress/](https://osp-kitchen.gitlab.io/www.q-o2.oscillation.public-adress/), this is where you can see the change for now (it is going to be re-mapped to the domain name oscillation-festival.be as soon as possible).

If the changes are not directly visible it is possible that your browser cached the page and thus is not requesting the new version. If you want to be sur to avoid that you can do an hard refresh with `ctrl+shift+r` when you have the webpage opened in your browser, this will force removing the cache and request the last versions.

### Markdown and Yaml

Every file is divided in two sections.
It begins with meta-data section encoded in yaml which begins and ends with `---`.
It is followed by the text encoded in markdown.

The markdown section is for the main text content (like the _about_).
The yaml section is for meta-data and more singular content (like the _agenda_ or the _line-up_).

**Markdown** allows you to type text divided in paragraphs (by following two line breaks).
Here is a [guide](https://www.markdownguide.org/cheat-sheet/) on how to things like header, bold, links, or list in markdown.
Note that for now we have no bold or italic fonts.

**Yaml** is more structurated and requiere more care in term of syntax.
It is made out of key and value pairs, a value can contains others key value pairs creating a nested structure.

### YAML Agenda

It is important that in the agenda:
* day's two-digit date are inside `'`, preceded by `-` and followed by `:`, like so `- '26.04':`
* same for events names, like so `- 'Hildegard Westerkamp':`
* same for links names, like so `- 'Artist website':`
* `description` and `biography` are followed by `|` and are NOT inside `'`, this also require the text to start on a new line and with one more indentation level than the field. Those are parsed as markdown, so multiple paragraphs, bold, links, list, etc.
```YAML
biography: |
    here's a first **biography**

    here's a second one

    and a third
links:
- 'Vimeo' : http://...
```

To add an image url, your write it like this: 
```YAML
image: '/images/name_of_the_file.jpg'
```

### Newsletters

To add a new post:
1. create a new file in the `articles/` folder
2. start the file with this set of meta-data:

    ```YAML
    ---
    title: The Oscillation Bulletin Episode 1
    date: 04-28
    slug: news-1
    image: './images/UGj7yikg.jpeg'
    image_credits: '© Dmitri Djuric'
    section: blog
    status: draft
    ---
    ```
3. write the content in Markdown below
4. by default it will not appear! (this allow you do edit it and save it without it being published). **to actually post it, you have to remove the metadata line with `status: draft`.**

Notes:
* for every news increment the slug of one: `slug: news-1`, must become `slug: news-2`, etc.

To create a `fr ` translated version of the blog post:
1. do the same steps (creating a new file, etc) but in the `article_fr/` folder and with this set of meta-data:

    ```YAML
    ---
    title: Bulletin du festival oscillation, épisode 1
    lang: fr
    slug: news-1
    ---
    ```
    it is important that the `slug` is the same than the original (english) version. You don't have to precise any other meta-data.


### specific meta-data and website structure

The `default_state` meta-data controls if a section is by default (that is to say when the user opens the website).
It has 3 values:
* `'on'` visible on the page at the start
* `'off'` not on the page byt they can click on its button to toggle it
* `'disabled'` not accessible for now (can not even click the button to toggle it)

The website remember the user checkbox selection through the url. For example https://osp-kitchen.gitlab.io/www.q-o2.oscillation.public-adress/?shape=on&about=on ends with `?shape=on&about=on` meaning your current selection of visible section is only the shape/drawing and the about section on.
You can check/uncheck what you want and copy paste the url from the address bar to create a link to specific part of the website, for example to send a link to the agenda directly (in a mail, or social media post) you can make the link ends with `?shape=on&agenda=on`.


