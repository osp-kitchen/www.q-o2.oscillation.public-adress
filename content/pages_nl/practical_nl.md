---
title: Praktische info
slug: practical
lang: nl
---

## Locaties

**Q-O2** Koolmijnkaai 30-34, 1080 Sint-Jans-Molenbeek

**Decoratelier** Manchesterstraat 17, 1080 Sint-Jans-Molenbeek

**Atoma** Pierre Decosterstraat 110, 1190 Vorst

**Zonneklopper** Glasblazerijlaan 23A, 1190 Vorst

**Abdij van Vorst** Sint-Denijsplein 9, 1190 Vorst

**Q-O2**is een kunstenlaboratorium voor experimentele muziek en geluidskunst. Sinds 2006 beschikt de organisatie over een eigen ruimte in de Brusselse kanaalzone, waar residenties voor kunstenaars worden georganiseerd die focussen op artistiek onderzoek en reflectie, meer dan op de productie van afgewerkte werken. De publieke output van dit onderzoek kan verschillende vormen aannemen: tentoonstellingen, concerten, thematische projecten, symposia, publicaties, workshops, en het jaarlijkse Oscillation festival. Q-O2 zet collaboratieve en transversale methodes in, en staat open voor cross-overs naar andere disciplines, verschillende stedelijke en sociale contexten, en samenwerkingen met zowel lokale als internationale partners.

[q-o2.be](http://www.q-o2.be/nl/)

**Decoratelier** werd opgericht in 2016 door Jozef Wouters en Menno Vandevelde. Het is gevestigd in een oud fabriekspand in Brussel, en is de uitvalsbasis voor diverse projecten en artistieke samenwerkingen, en een laagdrempelige werkplek voor kunstenaars uit verschillende disciplines. De ruimte wordt constant uitgeprobeerd, en treedt in conversatie met hout, ijzer en karton. Het is de permanente plek binnen een constructieve zoektocht naar steeds veranderende ruimtes waarin kunst, denken en ambacht elkaar ontmoeten. 

[somethingatdecoratelier.brussels](http://somethingatdecoratelier.brussels)

**ATOMA** is een leef- collectief beheerde ruimte met ateliers (hout, metaal, muziek, geluid, beeldend, fotografie, zeefdruk, brouwerij, bakkerij, chocolade...) en een leefruimte. Zoals elke collectief beheerde ruimte, zoekt ATOMA naar methodes om samenwerking te organiseren - soepel en oprecht - over hoe samen te zijn, hoe met elkaar te praten, ... Atoma is tevens een publieke ruimte: tijdens evenementen, die variëren van concerten tot screenings en evenementen in het kader van workshops van Atoma, komt de subtiele sfeer van ervaringen delen en experimenteren met anderen naar boven.

**Zonneklopper** is een collectief project ontstaan rond een tijdelijke bezetting in Vorst, waar gexperimenteerd wordt en gewerkt wordt aan nieuwe vormen van zelforganisatie voor activisme, kunst, cultuur en maatschappij. Het fungeert als een gevarieerd en divers dorp waar regelmatig publieke evenementen zoals concerten, performances en tentoonstellingen worden georganiseerd, kunstenaarsateliers worden voorzien en werkruimten worden gedeeld, dat ook dienst als huisvesting en onderdak voor migranten op doorreis. Zonneklopper wordt autonoom beheerd en alle middelen worden gedeeld en onderling verdeeld in collectief belang.

[zonneklopper.net](http://www.zonneklopper.net)


## Credits

Oscillation is een festival georganiseerd door [Q-O2.be](https://Q-O2.be).

Q-O2 wordt gesubsidieerd door de Vlaamse Overheid en de Vlaamse Gemeenschapscommissie.

Web development & design door [OSP](http://osp.kitchen).
Deze site gebruikt MetaAccanthis van Amélie Dumont, en Karrik van Jean-Baptiste Morizot & Lucas Le Bihan, beide onder de SIL Open Font Licentie.

Radio partners: Shirley & Spinoza, The Listening Arts Channel, Cashmere Radio, Kiosk Radio, Radio LYL, Radio Helsinki, Radio Panik, Radio Campus Bruxelles.

Festival Team: Julia Eckhardt, Caroline Profanter, Henry Andersen, Ludo Engels, Dries Robbe, Margherita Brillada, Amar Ruiz, en Christel Simons.

Contact: info@q-o2.be

[Vorige edities](http://www.oscillation-festival.be/start.html)
