---
title: Alisa Oleva
section: specials
---
Wherever you are in the world, this collective **[Listening diary](https://pad.riseup.net/p/6hX9WHSBMo-wrZCeT0Tj)** is an invitation to pause, listen and note down what you hear in this precise moment of time. You can take just a moment to listen or you can choose to do it for 10 minutes, half an hour or more. You can come back any time on a different day as well. You are also welcome to come and just read what others are hearing. 

