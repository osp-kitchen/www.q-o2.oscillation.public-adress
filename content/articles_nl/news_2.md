---
title: The Oscillation Bulletin Episode 2
date: 04-15
lang: nl
slug: news-2
---

Dearest readers,

Welcome to the second episode of our Oscillation ::: Public Address bulletin. In this communiqué, we’d like to highlight our extensive program of workshops and walks; be sure to subscribe for them in time, since places are limited!

On Tuesday, April 26, Den Haag-based sound artist and electroacoustic music composer **Margherita Brillada** will kick off the workshop program with her Radiophonix workshop, in which we dive deep into radio art, broadcasting technologies and discuss our own radiophonic pieces.

The day after, the infamous **RYBN** collective arrive at our place and start a two-day open lab, where they will plot, trouble-shoot and discuss their walks focussing on The Offshore Tour Operator. Public visits in the open lab are welcome with appointment, by e-mailing to info@rybn.org. Friday, saturday and sunday walks organized by RYBN bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels. On sunday afternoon, the collective will summarise their experiences of the past week, realising the Offshore Tour Operator in Brussels.

Also on the program April 27: a Whip Cracking workshop by **Lia Mazzari**, teaching participants to swing and crack whips as an investigatory sonic mapping device to activate the architectures and sites around us. Later that day, **Elena Biserna's** workshop aims to be a platform to reflect together on gendered (listening) experiences in public space and to unlearn some of the behaviours that are assumed as appropriate, safe or expected when we walk.

Furthermore, **Alisa Oleva** will host no less than 3 sessions: a walkshop about the sound of a city, a collective walk following a shared score and a more individual workshop, discussing the sounds somebody in Ukraine is hearing the present day. **Attila Faravelli** offers a presentation and a practical exploration of the Aural Tools on friday, which consist in a series of objects designed to produce and broadcast sound in ways that traditional recorded media (LPs, CDs, digital) cannot. On sunday, **Jérôme Giller** proposes to survey the bottom of Vorst/Forest by connecting blocks of habitation that allow us to understand the historical, sociological and economic evolution of of the industrial area, distributed around the railway line 124 which connects Brussels to Charleroi.

And lastly: Caroline Profanter and Margherita Brillada **[previewed](https://www.mixcloud.com/KioskRadio/volume-presents-oscillation-w-caroline-profanter-margherita-brillada-kiosk-radio-14042022/)** the festival this week on Kiosk Radio: get hyped!

Kind regards,

The Oscillation Crew
