---
title: The Oscillation Bulletin Episode 6
date: 04-29
slug: news-6
image: './images/IMG-8048.jpg'
image_credits: 'Lázara Rosell Albear last night at Decoratlier. © Elena Biserna'
section: blog
---

Dear Q-O2 aficionados,

Still dreaming about last night's dazzling performances under Decoratelier's mesmerizing night sky, it's already time to look ahead ::: we're going to Atoma tonight. As per last night, doors open at 18:00, and the first act starts at 19:00 - tickets are still available at the door. For those unable to make it tonight: all acts are live relayed on our website, and surely don't hesitate to get in touch with other listeners via the chat.

The first performance is a site-specific action by Collective Actions Group that combines video and typescript documentation made in the Depot station on the Savelovskaya line in January 1990, originally made for an unrealised installation by the grandfather of Russian performance art, Andrei Monastyrsky. For Oscillation, this video, as well as other typescript documentation from the series Trips Out of Town, serve as a setting for a reading choreography to be realised in collaboration with the event's organisers. Performed by Sabine Hänsgen, Elena Biserna and Henry Andersen.

At 19:30, Bill Dietz will test our festival sound system by giving a lecture performance My Ears, The Police at and about the legal limits of sound: 'If listening could play a role in rethinking the public sphere, its mediations, complicities, and normalizing mechanisms would first need to be better articulated. To that end, the legal, infrastructural, and conceptual limits of listening, both those local to Brussels and to the zombie remnants of 'modern listening' as such, are amplified into audibility.'

Next up are Ryoko Akama & Anne-F Jacques. Filaments warming up, the slow deformation of materials, thermostats as controllers. In this performance, Akama and Jacques explore heat as an active force at work around us. Through sound, light, and barely perceptible movement, temperature becomes a noticeable presence in the performance.

Matana Roberts is taking the stage at 21:15. Roberts is an internationally renowned composer, band leader, saxophonist, sound experimentalist and mixed-media practitioner, best known for her acclaimed Coin Coin project, a multi-chapter work of 'panoramic sound quilting'.

Next up is Marta De Pascalis, an Italian musician and sound-designer based in Berlin. Her solo works employ analogue synthesis and a tape-loop system, which is used to create patterns of repetition that shape a sense of a dense, dynamic and cathartic distance. Her sound touches a wide range of electronic music genres, including ambient, Berlin school first excursions, psychedelic, and tape music.

This night's closer is the Open Mic Night, that will presumably take the form of a collective stroll down Expectation Avenue, guided from one performance to the next by the assumptions of Francesca Hawker, who will hopefully have some prior knowledge about what will occur. Probably, the event will last for about an hour (excluding breaks) and 5 performers will have 5 minutes each to deliver a well-adjusted and surprising offering to the audience.

It's friday, I'm in love,

The Oscillation Crew
