#!/usr/bin/env python
# -*- coding: utf-8 -*- #

from markdown import Markdown, markdown
from markdown3_newtab import NewTabExtension


AUTHOR = 'Q-02'
SITENAME = 'Oscillation'
SITEURL = 'oscillation-festival.be/2022'
# SITEURL = 'osp-kitchen.gitlab.io/www.q-o2.oscillation.public-adress'

PATH = 'content'

TIMEZONE = 'Europe/Bruxelles'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# CUSTOM PAREMETERS
# ==============================================================================

# theme path
THEME = 'theme'


# ARCHITECTURE
# ==============================================================================

ARTICLE_SAVE_AS = ''
ARTICLE_URL = ''
ARTICLE_LANG_SAVE_AS = ''
ARTICLE_LANG_URL = ''

# pages have no category
PAGE_SAVE_AS = ''
PAGE_URL = ''
PAGE_LANG_SAVE_AS = ''
PAGE_LANG_URL = ''


SLUGIFY_SOURCE = 'title'

# disable the generation of those default html files
CATEGORY_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
TAG_SAVE_AS = ''

# the "collections" type html pages that we want
# DIRECT_TEMPLATES = ['index', 'authors', 'categories', 'tags', 'archives']
# DIRECT_TEMPLATES = ['index', 'external-comm', 'individual-comm']
ARTICLE_PATHS = ['articles', 'articles_fr', 'articles_nl']
PAGE_PATHS = ['pages', 'pages_fr', 'pages_nl']
DIRECT_TEMPLATES = ['index', 'index_fr', 'index_nl', 'stories']

# DATE
# ==============================================================================

TIMEZONE = 'Europe/Paris'
# by default pelican asks for a date for every article (?)
DEFAULT_DATE = 'fs'

DEFAULT_DATE_FORMAT = '%a %-d %b'
#only change how article.locale_date is displayed
#the metadate is still YYYY MM DD
LOCALE = ('be')
#change the lang in which days and month are displayed

# TRANSLATION
# ==============================================================================

# only article in this lang will be listed by default
DEFAULT_LANG = 'en'
# two article with same slug are considered as translation:
# ARTICLE_TRANSLATION_ID = 'slug'
# by default slug is generated from title:
# SLUGIFY_SOURCE = 'title'
# which means the translated article have to have the same title
# but the title is going to be translated, so by default
# the best way is to setup slug metadata manually so it fit the english slugification
PAGE_TRANSLATION_ID = 'slug'

# MARKDOWN EXTENSION
# ==============================================================================

MARKDOWN = {
    'extensions': [
        NewTabExtension(),
    ],
    'extension_configs': {
    },
    'output_format': 'html5',
}

# PLUGINS
# ==============================================================================

PLUGIN_PATHS = ['plugins']
PLUGINS = ['yaml_metadata', 'image_process']
#  from https://github.com/pR0Ps/pelican-yaml-metadata


# to parse some mutliline yaml field as markdown
def md_parse(value):
    return markdown(value, extensions=MARKDOWN['extensions'], extension_configs=MARKDOWN['extension_configs'])

JINJA_FILTERS = { 
    'md_parse': md_parse
}

# --- image_process (from: https://github.com/pelican-plugins/image-process)
# note: we can add the class manually in the html template (and not in the markdown)
IMAGE_PROCESS = {
    # reminder on srctset
    # https://www.youtube.com/watch?v=2QYpkrX2N48&ab_channel=KevinPowell
    # our agenda <img> html elements are 33vw at max (they can be smaller when 3 col, or when height is limiting)
    # so 1280px on 2x devices
    "agenda": {
        "type": "responsive-image",
        # Here we could actually precise the size of the <img> html element with media query
        "sizes": (
            "(min-width: 920px) 33vw, "
            "100vw"
        ),
        "srcset": [
            # first parameter is to inform the browser of the image file size (with the w unit)
            # or just precising the display subpixel type
            # !before! it has to actually download the image
            ("640w", ["scale_in 640 640 True"]),    # 1/3 of 1920px
            ("1280w", ["scale_in 1280 1280 True"])   # 1/3 of 3840px
        ],
        "default": "640w"
    }
}