---
title: The Oscillation Bulletin Final Episode
date: 05-4
slug: news-9
image: './images/Webp.net-resizeimage__1_.jpg'
image_credits: 'Jasmine Guffond at Zonneklopper. © Nikolaj Jessen'
section: blog
---

Dear readers,

As we're writing this, the post-Oscillation blues still hang gently over a sunny Q-O2 office. What a glorious edition it has been; all the performances, walks, talks, workshops and countless encounters feel like one long-stretched hazy highlight, impossible to pick just one or two.

It seems the appropriate time to do a big round of thank-yous: hence, thanks to all the location part­ners and people involved (Decoratelier! Atoma! Zonneklopper!) who helped to make this pos­si­ble, to all the artists who in one way or anoth­er pro­vid­ed works and per­for­mances, to our radio partners, to the volunteers, and as last, but not least, to all of you who joined us in this grand four day celebration. In the coming weeks we will post some of the recordings made throughout the festival our Soundcloud, and, some of the festival specials are still available on our website.

Hoping to see you next year,

The Oscillation Crew
