---
title: The Oscillation Bulletin Episode 3
date: 04-25
slug: news-3
lang: fr
image: './images/unnamed__1_.jpg'
section: blog
---

Tune in, drop out!

Oscillation ::: Festival wouldn’t be Oscillation ::: Festival if there weren’t some radio waves involved; hence, a bulletin dedicated to all things radio, to get you prepared at the start of the festival (that is, only in three days!). Be sure to grab your tickets and reserve for the workshops and walks in time.

**Get in the mood** for our four day celebration: listen to festival previews on Klara's [Late Night Shift](https://radioplus.be/#/klara/herbeluister/7decc5cb-d953-11e5-9d78-00163edf48dd/5aa304db-c1bf-11ec-aee4-02b7b76bf47f/) (the Oscillation part starts at 1h 22’) by Bart Vanhoudt and Gerrit Valckenaers, [Kiosk Radio](https://www.mixcloud.com/KioskRadio/volume-presents-oscillation-w-caroline-profanter-margherita-brillada-kiosk-radio-14042022/) by Caroline Profanter and Margherita Brillada and [Cashmere Radio](https://cashmereradio.com/) (Tuesday, 15:00 Brussels time) /[ We Are Various](https://wearevarious.com/) (Wednesday, 17:00 Brussels time) by Margherita Brillada.

During the festival, **all of the acts are live relayed via the festival website**. Our radio partners Listening Arts channel, radio P-node and Shirley & Spinoza are also broadcasting parts of the program. The acts that are streamed are indicated with the red dot on the festival website. The first act starts Thursday, 19:00 Brussels time.

We also like to highlight **RYBN** as a very special addition to this year’s festival. We came across the practice of RYBN while researching contemporary sound-walking as part of an upcoming book in collaboration with Elena Biserna. The collective has been operating in Paris since 1999 in a wide variety of formats and guises, drawing on computer programming, activism and experimental art. There is a forensic precision to their work, paired with an expansive attitude to research and presentation, drawing parallels and comparisons across various systems and technologies that shape the world we live in.

For their contribution to Oscillation ::: Public Address, RYBN proposed to stroll through the murky world of international finance. **The Offshore Tour Operator** is a Situationist Dérive charting the opaque territories of Offshore Finance. The project starts out with the Paradise Papers, a 2017 leak of some 13.4 million documents detailing international tax evasion. This evasion was made possible by a vast network of so called “shell companies”, which use physical addresses all across the world to move money in and our of tax jurisdictions. On Friday, Saturday and Sunday, RYBN will lead a performative hunt for shell companies, trust firms, domiciliation agencies, and shadow finance offices—in short, the physical infrastructure of international tax evasion, reshaping the city by virtue of its shadow finance. The walks make use of specially designed players built by the collective to capture and document these addresses.

This is a rare and intimate occasion to encounter the work of RYBN, so don’t wait too long to sign up for **a walk and discussion on Friday, Saturday or Sunday**, followed by a collective discussion in various locations around Brussels.

Lastly, Den Haag-based sound artist and electroacoustic music composer Margherita Brillada will kick off the workshop program with her Radiophonix workshop, in which we dive deep into radio art, broadcasting technologies and discuss our own radiophonic pieces. Be sure to subscribe for this and the other workshops in time.

Hoping to meeting you later this week,

The Oscillation Crew
