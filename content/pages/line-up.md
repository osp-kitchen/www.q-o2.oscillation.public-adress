---
title: Line-up
default_state: 'off'
column: 2
order: 4
overflow: scroll

artists:
    - Alisa Oleva 
    - Aymeric de Tapol 
    - Bill Dietz 
    - BMB con.
    - Céline Gillain
    - Collective Actions Group 
    - David Helbich 
    - Davide Tidoni 
    - De zwarte zusters
    - DJ Marcelle 
    - Elena Biserna 
    - Enrico Malatesta & Attila Faravelli
    - Fausto Caceres (Shirley & Spinoza) 
    - Francesca Hawker 
    - Hildegard Westerkamp 
    - Jasmine Guffond 
    - Jérôme Giller 
    - Kate Carr 
    - Lázara Rosell Albear 
    - Leandro Pisano 
    - Lia Mazzari 
    - Margherita Brillada
    - Mariam Rezaei 
    - Marta De Pascalis 
    - Matana Roberts 
    - Pak Yan Lau & Amber Meulenijzer
    - Peter Kutin & Stan Maris
    - RYBN
    - Ryoko Akama & Anne-F Jacques
    - Thomas Ankersmit

---
