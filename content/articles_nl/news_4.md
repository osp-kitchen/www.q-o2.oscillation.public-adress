---
title: The Oscillation Bulletin Episode 4
date: 04-27
slug: news-4
lang: nl
image:
section: blog
---

Dear connoisseur of the good things in life,

For a festival that is taking place in some of Brussel's most exciting collectively-run spaces, meanwhile exploring alternative formats for sound in (the surrounding) public space, a bulletin on the (public) spaces and how we will activate them can't be missing.

On Thursday, the ever-inspring **Decoratelier** will be our host. Located in an old factory building in Molenbeek, it is the base for various projects and artistic collaborations and an accessible workplace for artists from various disciplines. Space is tested, conversations take place in wood, iron and cardboard. It is a permanent place within the constructional quest for constantly changing spaces in which art, thought and artisans meet.

Friday, we will settle at **ATOMA** in Forest, a collectively-run space with workshops (wood, metal, music, sound, visual, photography, screen printing, brewery, bakery, chocolate...) and a living space. Like any collectively-run space, ATOMA looks for methods for collaborative organisation, fluid and sincere, on how to be together and how to talk to each other. Atoma is also a public space; during its events—which range from concerts to screenings to events linked to the workshops of the space—the fragile notion of experience and experiment with others emerges.

The last two days are taking place at **Zonneklopper**, a collective project formed around a temporary occupation in Forest where people experiment and shape new forms of self-organisation for activism, art, culture, and society. It can be seen as a varied and diverse village where public events such as concerts, performances and exhibitions are regularly hosted, artist’s ateliers are provided, and working spaces are shared, together with housing and accommodation for migrants in transit. Zonneklopper is self-managed and all the resources are shared and mutualised in a collective perspective.

On Saturday and Sunday afternoon, 3 open air performances per day will take place in the public spaces surrounding the **Abbaye De Forest**: Amber Meulenijzer and Pak Yan Lau will create a composition and live performance tailored to the infamous SAAB sound system, De zwarte zusters will create an improvisation on sound, image and their troupe as a community, Lia Mazzari will perform Whipping Music with the participants of her workshop, BMB con. is developing a new site-specific performance and David Helbich is setting up his Figures of Walking Together installation.

An artist very worth highlighting in the context of public listening is **Alisa Oleva**. She has a highly unique practice which tries to rethink walking practice within a contemporary, globalised world. There is an intimacy and a precision in Oleva’s work that sets it apart from many other walking practices. Growing up as a teenager in post-soviet Moscow, she took part in a nascent subculture of urban exploration (“urbex”) that blossomed up in the city’s newly deregulated urban sphere. Later, living in London, she connected these formative experiences with traditions of sound walking and psychogeography. Many of Oleva’s woks try to connect walkers in different cities via phone lines, drawing on communities of hers in Kiev, Moscow, London, Istanbul.

For Oscillation, don't miss the rare occasion to join of the **3 walks she is guiding**: one on the experiencing of Brussels' (sound) environement, one that focusses on the collective act of listening and that questions individual listening. 

T-t-t-t-tommorow is only one night away,

The Oscillation Crew
